# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

## Author ##
Ryan Gurnick
rgurnick@uoregon.edu

### Objectives ###

The objectives of this mini-project are:

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

### Features ###

- Routing to the document root (`docroot`) specified within the `credentials.ini`
- Restrict access to //, ~, and .. prefixes and return a 403 if found
- HTML character escaping for the endpoint to ensure that there are not html injections.


### Tests ###

There is a minor set of tests that go through each of the cases that are required. They also check the functionality discussed above. 